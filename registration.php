<?php
// Check if the form has been submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Include your database connection file
    include("config.php"); // Replace "db_connection.php" with your actual database connection script
   
    // Get data from the form
    $firstname = $_POST["Firstname"];
    $lastname = $_POST["Lastname"];
    $gender = $_POST["gender"];
    $email = $_POST["email"];
    $password = $_POST["password"];
    $number = $_POST["number"];
    
    // Perform data validation and sanitization here if needed
    
    // Create an SQL INSERT statement to insert the data into the database
    $sql = "INSERT INTO registration (Firstname, Lastname, gender, email, password, number) 
            VALUES ('$firstname', '$lastname', '$gender', '$email', '$password', '$number')";
    
    // Execute the SQL statement
    if (mysqli_query($conn, $sql)) {
        echo "Registration successful!";
        header("Location: Hospitals.php");
    } else {
        echo "Error: " . mysqli_error($conn);
    }
    
    // Close the database connection
    mysqli_close($conn);
} else {
    // If the form wasn't submitted, display an error or redirect as needed
    echo "Form submission error.";
}
?>





<!DOCTYPE html>
<html>
<head>
    <title>ZANZIBAR DISEASE TRACKING SYSTEM</title>
    <style>
        body {
            font-family: Arial, sans-serif;
            background-color: #f0f0f0;
            margin: 0;
            padding: 0;
        }

        h1 {
            text-align: center;
            color: #333;
        }

        form {
            max-width: 400px;
            margin: 0 auto;
            background-color: #fff;
            padding: 20px;
            border-radius: 8px;
            box-shadow: 0 0 10px rgba(0, 0, 0, 0.2);
        }

        label {
            display: block;
            font-weight: bold;
            margin-bottom: 10px;
        }

        input[type="text"],
        input[type="number"],
        select,
        input[type="email"],
        input[type="password"] {
            width: 100%;
            padding: 10px;
            margin-bottom: 20px;
            border: 1px solid #ccc;
            border-radius: 4px;
        }

        input[type="submit"] {
            background-color: #333;
            color: #fff;
            padding: 10px 20px;
            border: none;
            border-radius: 4px;
            cursor: pointer;
        }

        input[type="submit"]:hover {
            background-color: #555;
        }
    </style>
</head>
<body>
    <h1>Registration form</h1>
    <form  method="POST">
        <label for="Firstname">First name:</label>
        <input type="text" name="Firstname" required>
        <label for="Lastname">Last name:</label>
        <input type="text" name="Lastname" required>

        <label for="gender">Gender:</label>
        <select name="gender" required>
            <option value="male">Male</option>
            <option value="female">Female</option>
            <option value="other">Other</option>
        </select>
        <label for="email">Email:</label>
        <input type="email" name="email" required>

        <label for="password">Password:</label>
        <input type="password" name="password" required>
        <label for="number">Phone number:</label>
        <input type="number" name="number" required>

        <input type="submit" value="Register">
    </form>
</body>
</html>
