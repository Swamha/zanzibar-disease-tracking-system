<?php
session_start();
require_once("config.php"); // Include your database connection file

// Check if the form is submitted
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // Retrieve form data
    $hospitalID = $_POST["number"];
    $hospitalName = $_POST["hospitalname"];
    $hospitalAddress = $_POST["hospitaladdress"];

    // Insert data into the database
    $sql = "INSERT INTO hospital (hospitalID, Hospital_name, Address)
            VALUES ('$hospitalID', '$hospitalName', '$hospitalAddress')";

    if ($conn->query($sql) === TRUE) {
        echo "Data saved successfully!";
    } else {
        echo "Error: " . $sql . "<br>" . $conn->error;
    }
}

// Fetch and display data from the database
$sql = "SELECT hospitalID, Hospital_name, Address FROM hospital";
$result = $conn->query($sql);

?>







<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
    <meta name="generator" content="Hugo 0.84.0">
    <title>ZANZIBAR DISEASE TRACKING SYSTEM</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/5.0/examples/dashboard/">
    <link rel="stylesheet" href="dashboard.php">
    <link rel="stylesheet" href="hospitals.php">
    <link rel="stylesheet" href="buy.php">



    

    <!-- Bootstrap core CSS -->
<link href="bootstrap.min.css" rel="stylesheet">

    <style>
      .bd-placeholder-img {
        font-size: 1.125rem;
        text-anchor: middle;
        -webkit-user-select: none;
        -moz-user-select: none;
        user-select: none;
      }

      @media (min-width: 768px) {
        .bd-placeholder-img-lg {
          font-size: 3.5rem;
        }
      }
    </style>

    
    <!-- Custom styles for this template -->
    <link href="dashboard.css" rel="stylesheet">
  </head>
  <body>
    
<header class="navbar navbar-dark sticky-top bg-dark flex-md-nowrap p-0 shadow">
  <a class="navbar-brand col-md-3 col-lg-2 me-0 px-3" href="dashboard.php"></a>
  <button class="navbar-toggler position-absolute d-md-none collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#sidebarMenu" aria-controls="sidebarMenu" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <input class="form-control form-control-dark w-100" type="text" placeholder="Search" aria-label="Search">
  <div class="navbar-nav">
    <div class="nav-item text-nowrap">
      <a class="nav-link px-3" href="index.php">Sign out</a>
    </div>
  </div>
</header>

<div class="container-fluid">
  <div class="row">
    <nav id="sidebarMenu" class="col-md-3 col-lg-2 d-md-block bg-light sidebar collapse">
      <div class="position-sticky pt-3">
        <ul class="nav flex-column">
          <li class="nav-item">
            <a class="nav-link active" aria-current="page" href="dashboard.php">
              <span data-feather="home"></span>
              Dashboard
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="Hospitals.php">
              <span data-feather="file"></span>
              Hospital
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="patient.php">
              <span data-feather="shopping-cart"></span>
              Patient
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="doctor.php">
              <span data-feather="users"></span>
              doctor
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="medical_test.php">
              <span data-feather="bar-chart-2"></span>
              Medical_test
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="disease.php">
              <span data-feather="layers"></span>
              Disease
            </a>
          </li>
        </ul>

        <h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
          <span>Treatment</span>
          <a class="link-secondary" href="patient_treatment.php" aria-label="Add a new report">
            <span data-feather="plus-circle"></span>
          </a>
        </h6>
        <ul class="nav flex-column mb-2">
          <li class="nav-item">
            <a class="nav-link" href="patient_record.php">
              <span data-feather="file-text"></span>
              patient_record
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="patient_test.php">
              <span data-feather="file-text"></span>
              patient_test
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="hospital_patient.php">
              <span data-feather="file-text"></span>
              Hospital_patient
            </a>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="hospital_doctor.php">
              <span data-feather="file-text"></span>
              Hospital_doctor
            </a>
          </li>
        </ul>
      </div>
    </nav>

    <main class="col-md-9 ms-sm-auto col-lg-10 px-md-4">
      <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <h1 class="h2">Hospital</h1>
        <div class="btn-toolbar mb-2 mb-md-0">
          <div class="btn-group me-2">
            <button type="button" class="btn btn-sm btn-outline-secondary">Share</button>
            <button type="button" class="btn btn-sm btn-outline-secondary">Export</button>
          </div>
          <button type="button" class="btn btn-sm btn-outline-secondary dropdown-toggle">
            <span data-feather="calendar"></span>
            This week
          </button>
        </div>
      </div>
      <br>
      <br>
      <br>


      <div class="content">
    <form method="post">
    <div class="mb-3">
      <label for="formGroupExampleInput" class="form-label">Hospital ID:</label>
      <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Enter Hospital ID" name="number">
    </div>
    <div class="mb-3">
      <label for="formGroupExampleInput2" class="form-label">Hospital Name:</label>
      <input type="text" class="form-control" id="formGroupExampleInput2" placeholder="Enter HOpital Name" name="hospitalname">
    </div>
    <div class="mb-3">
      <label for="formGroupExampleInput" class="form-label">Hospital Address:</label>
      <input type="text" class="form-control" id="formGroupExampleInput" placeholder="Enter Hospital Address" name="hospitaladdress">
    </div>
    <div style="float: right;">
      <input type="submit">
    </div>
    </form>`    
    </div>


    <!-- Display the saved data in an HTML table -->
<!-- Display the saved data in an HTML table -->
<table border="1">
    <tr>
        <th>Hospital ID</th>
        <th>Hospital Name</th>
        <th>Hospital Address</th>
    </tr>
    <?php
    if ($result !== FALSE) {
        if ($result->num_rows > 0) {
            // Fetch and display data
            while ($row = $result->fetch_assoc()) {
                echo "<tr>";
                echo "<td>" . $row["hospitalID"] . "</td>";
                echo "<td>" . $row["Hospital_name"] . "</td>";
                echo "<td>" . $row["Address"] . "</td>";
                echo "</tr>";
            }
        } else {
            echo "<tr><td colspan='3'>No data found.</td></tr>";
        }
    } else {
        echo "<tr><td colspan='3'>Error fetching data.</td></tr>";
    }
    ?>
</table>
<style>
    table {
        width: 100%;
        border-collapse: collapse;
        margin: 0 auto;
    }

    th, td {
        padding: 12px 16px;
        text-align: left;
        border-bottom: 1px solid #ddd;
    }

    th {
        background-color: #f2f2f2;
        font-weight: bold;
    }

    tr:hover {
        background-color: #f5f5f5;
        transition: background-color 0.3s ease;
    }

    /* Add more styles as needed */
    
    /* Add a border around the table */
    table {
        border: 1px solid #ddd;
    }

    /* Style header text */
    th {
        text-transform: uppercase;
        letter-spacing: 1px;
    }

    /* Add alternating row colors */
    tr:nth-child(even) {
        background-color: #f9f9f9;
    }

    /* Style links within table cells */
    td a {
        text-decoration: none;
        color: #0077cc;
        font-weight: bold;
    }

    /* Center align text in table cells */
    td {
        text-align: center;
    }

    /* Add a subtle box shadow on hover */
    tr:hover {
        box-shadow: 0 0 10px rgba(0, 0, 0, 0.1);
    }
</style>



    <script src="../assets/dist/js/bootstrap.bundle.min.js"></script>

      <script src="https://cdn.jsdelivr.net/npm/feather-icons@4.28.0/dist/feather.min.js" integrity="sha384-uO3SXW5IuS1ZpFPKugNNWqTZRRglnUJK6UAZ/gxOX80nxEkN9NcGZTftn6RzhGWE" crossorigin="anonymous"></script><script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js" integrity="sha384-zNy6FEbO50N+Cg5wap8IKA4M/ZnLJgzc6w2NqACZaK0u0FXfOWRRJOnQtpZun8ha" crossorigin="anonymous"></script><script src="dashboard.js"></script>
  </body>
</html>
