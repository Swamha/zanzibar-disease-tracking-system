<?php
// Database connection
require_once("config.php");

// Get user input
$username = $_POST['username'];
$password = $_POST['password'];

// Perform SQL query
$sql = "SELECT * FROM users WHERE username = '$username' AND password = '$password'";
$result = $conn->query($sql);

if ($result->num_rows == 1) {
    // Login successful
    echo "Login successful!";
    header("Location: dashboard.php");
} else {
    // Login failed
    echo "Login failed. Please check your username and password.";
    // header("Location: error.php");
}

$conn->close();
?>



<!DOCTYPE html>
<html>
<head>
    <title>Login Page</title>
</head>
<body>
    <center><img class="mb-4" src="23.jpg" alt="" width="90" height="90"></center>
    <br><br>
    <form method="post">
        <label for="username">Username:</label>
        <input type="text" id="username" name="username" required><br><br>

        <label for="password">Password:</label>
        <input type="password" id="password" name="password" required><br><br>

        <input type="submit" value="Login">
        <h3><a href="registration.php">click here to register</a>
    </form>
</body>

<style>
        body {
            font-family: Arial, sans-serif;
            background-color: #3498db; /* New background color (blue) */
            color: #fff; /* Text color for better contrast */
        }

        h2 {
            text-align: center;
            color: #333;
        }

        form {
            max-width: 300px;
            margin: 0 auto;
            background-color: #fff;
            padding: 20px;
            border: 1px solid #ccc;
            box-shadow: 0px 0px 10px #aaa;
            border-radius: 5px;
        }

        label {
            display: block;
            margin-bottom: 10px;
            font-weight: bold;
            color: #333;
        }

        input[type="text"],
        input[type="password"] {
            width: 100%;
            padding: 10px;
            margin-bottom: 15px;
            border: 1px solid #ccc;
            border-radius: 3px;
        }

        input[type="submit"] {
            background-color: #333;
            color: #fff;
            padding: 10px 20px;
            border: none;
            border-radius: 3px;
            cursor: pointer;
        }

        input[type="submit"]:hover {
            background-color: #555;
        }
    </style>
</html>
